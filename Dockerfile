FROM node:14.15.5-alpine3.10

LABEL maintainer="rustem.yuzlibaev@yandex.ru"

WORKDIR /splash

COPY /backend/package.json /splash

RUN npm install

COPY /backend/server.js /splash
COPY /build /splash/build

EXPOSE 9000

CMD ["npm", "start"]